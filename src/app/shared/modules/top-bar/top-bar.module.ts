import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {ReactiveFormsModule} from '@angular/forms'
//Modules
import {MaterialModule} from 'src/app/material-module'
//Components
import {TopBarComponent} from 'src/app/shared/modules/top-bar/components/top-bar/top-bar.component'
import {RouterModule} from '@angular/router'

@NgModule({
  declarations: [TopBarComponent],
  exports: [TopBarComponent],
  imports: [CommonModule,
            ReactiveFormsModule,
            MaterialModule,
            RouterModule]
})
export class TopBarModule {}


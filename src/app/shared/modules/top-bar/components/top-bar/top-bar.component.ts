import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router'

import {AuthTokenStorageService} from 'src/app/auth/services/auth-token-storage.service'
import {UserService} from 'src/app/user/services/user.service'
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {UserInterface} from '../../../../../user/types/user_interface';

@Component({
  selector: 'iz-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {

  isLoggedIn = false
  user: UserInterface
  userSub: Subscription
  currentUser$: Observable<UserInterface>

  constructor(private authTokenStorageService: AuthTokenStorageService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit(): void{
    this.isLoggedIn = !!this.authTokenStorageService.getToken()
    if (this.isLoggedIn) {
      this.currentUser$ = this.userService.getCurrentUser().pipe(map(user => user))
    }
  }

  logout(): void {
    this.authTokenStorageService.logOut()
    this.router.navigate(['/main'])
  }

}

import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {ReactiveFormsModule} from '@angular/forms'
//Modules
import {MaterialModule} from 'src/app/material-module'
//Components
import {RegisterFormComponent} from 'src/app/shared/modules/register-form/components/register-form/register-form.component'
import {RouterModule} from '@angular/router'

@NgModule({
  declarations: [RegisterFormComponent],
  imports: [CommonModule,
            ReactiveFormsModule,
            MaterialModule,
            RouterModule],
  exports: [RegisterFormComponent],
})
export class RegisterFormModule {}

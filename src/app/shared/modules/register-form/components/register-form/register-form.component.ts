import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core'
import {FormBuilder, FormGroup} from '@angular/forms'

@Component({
  selector: 'iz-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  @Input('initialValue') initialValueProps

  @Output('registerSubmit') registerSubmitEvent = new EventEmitter()

  registerForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initializeForm()
  }

  initializeForm(): void {
    this.registerForm = this.fb.group({
      email: this.initialValueProps.email,
      username: this.initialValueProps.username,
      firstname: this.initialValueProps.firstname,
      lastname: this.initialValueProps.lastname,
      password: this.initialValueProps.password,
      confirmPassword: this.initialValueProps.confirmPassword
    })
  }

  onSubmit(): void {
    this.registerSubmitEvent.emit(this.registerForm.value)
  }
}

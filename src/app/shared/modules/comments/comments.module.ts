import {NgModule} from '@angular/core'
//Components
import {CommentsComponent} from 'src/app/shared/modules/comments/components/comments/comments.component'
//Modules
import {CommonModule} from '@angular/common'
import {MaterialModule} from 'src/app/material-module'
//Services
import {CommentsService} from 'src/app/shared/modules/comments/services/comments.service'

@NgModule({
  declarations: [CommentsComponent],
  imports: [CommonModule, MaterialModule],
  exports: [CommentsComponent],
  providers: [CommentsService]
})
export class CommentsModule {}

import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {environment} from 'src/environments/environment'

import {CommentsInterface} from 'src/app/shared/modules/comments/types/comments_interface'

@Injectable()
export class CommentsService {

  constructor(private http: HttpClient) {}

  addToCommentToPost(postId: number, message: string): Observable<any> {
    return this.http.post(`${environment.server_url}/comment/${postId}/create`, {
      message: message
    })
  }

  getToCommentToPost(postId: number): Observable<CommentsInterface[]> {
    return this.http.get<CommentsInterface[]>(`${environment.server_url}/comment/${postId}/all`)
  }

  deletePost(commentId: number): Observable<any> {
    return this.http.delete(`${environment.server_url}/comment/${commentId}/delete`)
  }

}

import {AfterViewInit, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {Subscription} from 'rxjs'

import {CommentsService} from 'src/app/shared/modules/comments/services/comments.service'

import {PostInterface} from 'src/app/shared/modules/posts/types/post_interface'

@Component({
  selector: 'iz-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit, OnDestroy {

  @Input('post') postProps: PostInterface
  @Input('index') indexProps: number
  @Input('ispost') isPostLoadedProps: boolean

  commentsSub: Subscription

  constructor(private http: HttpClient, private commentService: CommentsService) {}

  ngOnInit(): void {
    if (this.isPostLoadedProps)
        this.getCommentsToPosts()
  }

  ngOnDestroy(): void {
    if(this.commentsSub) this.commentsSub.unsubscribe()
  }

  postComment(message: string, post: any): void {
    this.commentService.addToCommentToPost(post.id, message)
                                  .subscribe( comment => {
                                    post.comments.push(comment)
                                  })
  }

  getCommentsToPosts() {
    this.commentsSub = this.commentService.getToCommentToPost(this.postProps.id)
                                          .subscribe(comments => {
                                            this.postProps.comments = comments
                                          })
  }
}

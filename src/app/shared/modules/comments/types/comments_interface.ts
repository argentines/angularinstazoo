export interface CommentsInterface {
    id: number | null
    message: string
    username: string
}

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {PostService} from 'src/app/shared/modules/posts/services/post.service'
import {Observable, Subscription} from 'rxjs'

import {UserInterface} from '../../../../../user/types/user_interface'
import {PostResponseInterface} from 'src/app/shared/modules/posts/types/post-response_interface'
import {map, tap} from 'rxjs/operators';
import {ImageUploadService} from '../../../../services/image-upload.service';
import {UserService} from '../../../../../user/services/user.service';
import {NotificationService} from '../../../../services/notification.service';

@Component({
  selector: 'iz-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {

  @Input('user') userProps: UserInterface

  isPostLoaded = false
  posts$: Observable<PostResponseInterface[] | null>

  postSub: Subscription
  imageSub: Subscription

  constructor(private postService: PostService,
              private imageService: ImageUploadService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.initializePosts()
  }

  ngOnDestroy(): void {
    if(this.postSub) this.postSub.unsubscribe()
    if(this.imageSub) this.imageSub.unsubscribe()
  }

  initializePosts(): void {
   this.posts$ = this.postService.getAllPosts()
                  .pipe(
                    map(post => post),
                    tap(post => {
                      this.getImagesToPosts(post),
                      this.isPostLoaded = true
                    })
                  )
  }

  getImagesToPosts(posts: PostResponseInterface[]) {
     posts.forEach(p => {
      this.imageSub = this.imageService.getImageToPost(p.id)
                    .subscribe(image => {
                        p.image = image.imageBytes
                    })
      })
  }

  likePost(post: any): void {
    console.log(post)

   if(!post.usersLiked.includes(this.userProps.username)) {
      this.postService.likePost(post.id, this.userProps.username).subscribe(() => {
        post.usersLiked.push(this.userProps.username)
        this.notificationService.showSnackBar('Liked!')
      })
    }else{ //dislike
      this.postService.likePost(post.id, this.userProps.username).subscribe(() => {
        const index = post.usersLiked.indexOf(this.userProps.username, 0)
        if (index > -1) {
          post.usersLiked.splice(index, 1)
        }
      })
    }
  }

  formatImage(img: any): any {
    if (img == null) {
      return null
    }
    return `data:image/jpeg;base64,${img}`
  }

}

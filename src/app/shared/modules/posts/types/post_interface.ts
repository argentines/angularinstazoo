import {CommentsInterface} from 'src/app/shared/modules/comments/types/comments_interface'

export interface PostInterface {
  id: number | null
  title: string
  caption: string
  location: string
  image: File | null
  likes: number | null
  usersLiked: string[] | null
  comments: CommentsInterface[] | null
  username: string | null
}

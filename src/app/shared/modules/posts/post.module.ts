import {NgModule} from '@angular/core'
//Components
import {PostsComponent} from 'src/app/shared/modules/posts/components/posts/posts.component'
//Modules
import {CommonModule} from '@angular/common'
import {MaterialModule} from 'src/app/material-module'
import {CommentsModule} from 'src/app/shared/modules/comments/comments.module'
//Services
import {PostService} from 'src/app/shared/modules/posts/services/post.service'

@NgModule({
  declarations: [PostsComponent],
  imports: [CommonModule, MaterialModule, CommentsModule],
  exports: [PostsComponent],
  providers: [PostService]
})
export class PostModule {}

import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {environment} from 'src/environments/environment'
import {PostResponseInterface} from '../types/post-response_interface';

@Injectable()
export class PostService {

  constructor(private http: HttpClient) {}

  createPost(post: any): Observable<any> {
    return this.http.post(`${environment.server_url}/post/create`, post)
  }

  getPostByCurrentUser(): Observable<any> {
    return this.http.get(`${environment.server_url}/posts`)
  }

  getPostById(id: number): Observable<any> {
    return this.http.get(`${environment.server_url}/post/${id}`)
  }

  getAllPosts(): Observable<PostResponseInterface[]> {
    return this.http.get<PostResponseInterface[]>(`${environment.server_url}/post/all`)
  }

  updatePost(user: any): Observable<any> {
    return this.http.post(`${environment.server_url}/post/update`, user)
  }

  deletePost(id: number): Observable<any> {
    return this.http.delete(`${environment.server_url}/post/${id}/delete`)
  }

  likePost(id: number, username: string): Observable<any> {
    return this.http.post(`${environment.server_url}/post/${id}/${username}/like`, null)
  }

  getUser(response: any) {
    return response.user
  }

}

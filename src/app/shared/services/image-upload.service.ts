import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {environment} from 'src/environments/environment'

@Injectable()
export class ImageUploadService {

  constructor(private http: HttpClient) {}

  uploadImageToUser(file: File): Observable<any> {
    const uploadData = new FormData()
    uploadData.append('file', file)
    return this.http.post(`${environment.server_url}/image/upload`, uploadData)
  }

  uploadImageToPost(file: File, postId: number): Observable<any> {
    const uploadData = new FormData()
    uploadData.append('file', file)
    return this.http.post(`${environment.server_url}/image/${postId}/upload`, uploadData)
  }

  getProfileImage(): Observable<any> {
    return this.http.get(`${environment.server_url}/image/profileImage`)
  }

  getImageToPost(postId: number): Observable<any> {
    return this.http.get(`${environment.server_url}/image/${postId}/image`)
  }
}

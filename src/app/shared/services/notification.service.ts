import {Injectable} from '@angular/core'
import {MatSnackBar} from '@angular/material/snack-bar'

@Injectable()
export class NotificationService {

  constructor(private snackbar: MatSnackBar) {}

  showSnackBar(message: string): void {
    this.snackbar.open(message, null, {
      duration: 2000
    })
  }

}

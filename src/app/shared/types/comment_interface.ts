export interface CommentInterface {
  id: number | null
  message: string
  username: string
}

import {NgModule} from '@angular/core'
//Components
import {IndexComponent} from './components/index/index.component'
//Modules
import {CommonModule} from '@angular/common'
import {PostModule} from 'src/app/shared/modules/posts/post.module'
//Services

@NgModule({
  declarations: [IndexComponent],
  imports: [CommonModule, PostModule],
  exports: [IndexComponent],
  providers: []
})
export class IndexModule {}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {map, tap} from 'rxjs/operators';
//Services
import {PostService} from 'src/app/shared/modules/posts/services/post.service'
import {UserService} from 'src/app/user/services/user.service'
import {CommentsService} from 'src/app/shared/modules/comments/services/comments.service'
import {NotificationService} from 'src/app/shared/services/notification.service'
import {ImageUploadService} from 'src/app/shared/services/image-upload.service'

import {PostInterface} from 'src/app/shared/modules/posts/types/post_interface'
import {UserInterface} from 'src/app/user/types/user_interface'

@Component({
  selector: 'iz-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  isUserDataLoaded = true
  user$: Observable<UserInterface>

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.initializeUser()
  }

  initializeUser(): void {
   this.user$ = this.userService.getCurrentUser().pipe(
                                        map(user => user),
                                        tap(() => this.isUserDataLoaded = true)
                                      )
  }

}

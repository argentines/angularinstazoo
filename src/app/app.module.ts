import {BrowserModule} from '@angular/platform-browser'
import {RouterModule} from '@angular/router'
import {AppRoutingModule} from './app-routing.module'
import {HttpClientModule} from '@angular/common/http'
import {NgModule} from '@angular/core'
//Components
import {AppComponent} from 'src/app/app.component'
//Modules
import {NoopAnimationsModule} from '@angular/platform-browser/animations'
import {AuthModule} from 'src/app/auth/auth.module'
import {ReactiveFormsModule} from '@angular/forms'
import {MaterialModule} from './material-module'
import {TopBarModule} from 'src/app/shared/modules/top-bar/top-bar.module'
import {UserModule} from 'src/app/user/user.module'
import {IndexModule} from 'src/app/index/index.module'
//Services
import {NotificationService} from 'src/app/shared/services/notification.service';
import { PostsComponent } from './shared/modules/posts/components/posts/posts.component'
import {PostModule} from './shared/modules/posts/post.module';
import {CommentsService} from './shared/modules/comments/services/comments.service';
import {ImageUploadService} from './shared/services/image-upload.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NoopAnimationsModule,
    AuthModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    UserModule,
    IndexModule,
    PostModule,
    TopBarModule
  ],
  providers: [NotificationService, CommentsService, ImageUploadService],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'

import {IndexComponent} from 'src/app/index/components/index/index.component'

import {AuthGuardService} from 'src/app/auth/services/auth-guard.service'

const routes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  /*{path: 'main', component: IndexComponent}*/
  {path: 'main', component: IndexComponent, canActivate: [AuthGuardService]}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService]
})
export class AppRoutingModule {}

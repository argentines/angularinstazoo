import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
//Modules
import {MaterialModule} from 'src/app/material-module'
import {UserRoutingModule} from 'src/app/user/user-routing.module'
//Components
import {ProfileComponent} from 'src/app/user/components/profile/profile.component'
import {EditUserComponent} from 'src/app/user/components/edit-user/edit-user.component'
//Services
import {UserService} from 'src/app/user/services/user.service'

@NgModule({
  declarations: [ProfileComponent, EditUserComponent],
  imports: [CommonModule, MaterialModule, UserRoutingModule],
  providers: [UserService]
})
export class UserModule {}

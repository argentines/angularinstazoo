import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserInterface} from '../../types/user_interface';
import {AuthTokenStorageService} from '../../../auth/services/auth-token-storage.service';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {NotificationService} from '../../../shared/services/notification.service';
import {ImageUploadService} from '../../../shared/services/image-upload.service';
import {UserService} from '../../services/user.service';
import {Observable, Subscription} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {EditUserComponent} from '../edit-user/edit-user.component';

@Component({
  selector: 'iz-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {

  user$: Observable<UserInterface>
  selectedFile: File
  userProfileImage$: Observable<File>
  previewImage: any
  userImgSub: Subscription

  constructor(private authTokenStorage: AuthTokenStorageService,
              private notificationService: NotificationService,
              private imageService: ImageUploadService,
              private userService: UserService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.user$ = this.userService.getCurrentUser().pipe(
                                        map(user => user)
                                      )

    this.userProfileImage$ = this.imageService.getProfileImage()
                                                .pipe(
                                                  map(userImage => userImage)
                                                )


  }

  ngOnDestroy() {
    if (this.userImgSub) this.userImgSub.unsubscribe()
  }

  onFileSelected(event): void {
    this.selectedFile = event.target.files[0]

    const reader = new FileReader()
    reader.readAsDataURL(this.selectedFile)
    reader.onload = () => {
      this.previewImage = reader.result
    }
  }

  openEditDialog(): void {
    const dialogUserEditConfig = new MatDialogConfig()
    dialogUserEditConfig.width = '400px'
    dialogUserEditConfig.data = {
      user: this.user$.subscribe()
    }

    this.dialog.open(EditUserComponent, dialogUserEditConfig)
  }

  formatImage(img: any): any {
    if (img == null) {
      return null
    }

    return `data:image/jpeg:base64,${img}`
  }

  onUpload(): void {
    if (this.selectedFile != null) {
      this.userImgSub = this.imageService.uploadImageToUser(this.selectedFile)
        .subscribe(() => this.notificationService.showSnackBar('Profile image updated successfully'))
    }
  }

}

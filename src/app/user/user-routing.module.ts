import {NgModule} from '@angular/core'
import {RouterModule, Routes} from '@angular/router'

import {ProfileComponent} from 'src/app/user/components/profile/profile.component'
import {AuthGuardService} from 'src/app/auth/services/auth-guard.service'


const routes: Routes = [
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService], children: [
      /*{path: '', component: ProfileComponent, canActivate: [AuthGuardService]},
      {path: 'add', component: ProfileComponent, canActivate: [AuthGuardService]}*/
   ]},
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}

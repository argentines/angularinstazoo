import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {environment} from 'src/environments/environment'

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {}

  getUserById(id: number): Observable<any> {
    return this.http.get<any>(`${environment.server_url}/user/${id}`)
  }

  getCurrentUser(): Observable<any> {
    return this.http.get<any>(`${environment.server_url}/user/`)
  }

  updateUser(user: any): Observable<any> {
    return this.http.post<any>(`${environment.server_url}/user/update`, user)
  }

  getUser(response: any) {
    return response.user
  }

}

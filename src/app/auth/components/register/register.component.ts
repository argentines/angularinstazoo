import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {Subscription} from 'rxjs'

import {AuthService} from 'src/app/auth/services/auth.service'
import {NotificationService} from 'src/app/shared/services/notification.service'

import {UserRegisterInputInterface} from 'src/app/auth/types/user_register_input_interface'

@Component({
  selector: 'iz-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  registerForm: FormGroup
  registerSub: Subscription

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private notificationService: NotificationService) { }

  ngOnInit(): void {
    this.initializeForm()
  }

  ngOnDestroy(): void {
    if (this.registerSub) this.registerSub.unsubscribe()
  }

  initializeForm(): void {
    this.registerForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.compose([Validators.required])],
      firstname: ['', Validators.compose([Validators.required])],
      lastname: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.required])]
    })
  }

  onSubmit(registerInput: UserRegisterInputInterface): void {
    this.registerSub = this.authService.register(registerInput).subscribe(
      res => {
        this.notificationService.showSnackBar("Successfully Registered!")
      },error => {
        this.notificationService.showSnackBar("Something went wrong during registration")
      }
    )
  }

}

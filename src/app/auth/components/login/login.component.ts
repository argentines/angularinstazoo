import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import {Router} from '@angular/router'
import {Subscription} from 'rxjs'
//Services
import {AuthService} from 'src/app/auth/services/auth.service'
import {AuthTokenStorageService} from 'src/app/auth/services/auth-token-storage.service'
import {NotificationService} from 'src/app/shared/services/notification.service'
//Interfaces
import {LoginRequestInterface} from 'src/app/auth/types/login-request_interface'
//Modules
import {MatInputModule} from '@angular/material/input'

@Component({
  selector: 'iz-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup
  loginSub: Subscription

  constructor(private authService: AuthService,
              private authTokenStorageService: AuthTokenStorageService,
              private notificationService: NotificationService,
              private router: Router,
              private fb: FormBuilder) {  }

  ngOnInit(): void {
    if (this.authTokenStorageService.getUser()) {
      this.router.navigate(['main'])
    }
    this.initializeForm()
  }

  ngOnDestroy(): void {
    if (this.loginSub) this.loginSub.unsubscribe()
  }

  initializeForm() :void{
    this.loginForm = this.fb.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    })
  }

  onSubmit(): void {
    const request: LoginRequestInterface = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password
    }
    this.loginSub = this.authService.login(request).subscribe(user => {
      this.authTokenStorageService.saveToken(user.token)
      this.authTokenStorageService.saveUser(user)

      this.notificationService.showSnackBar("Successfully logged in")
      this.router.navigate(['/'])
      window.location.reload()
    }, error => {
      this.notificationService.showSnackBar(`Error: ${error.message}`)
    })
  }
}

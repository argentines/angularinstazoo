export interface UserInterface {
  id: number
  email: string
  username: string
  firstname: string
  lastname: string
  bio: string
}

export interface UserRegisterInputInterface {
  email: string
  username: string
  firstname: string
  lastname: string
  password: string
  confirmPassword: string
}

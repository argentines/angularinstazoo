import {Injectable} from '@angular/core'
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router'
import {Observable} from 'rxjs'

import {AuthTokenStorageService} from 'src/app/auth/services/auth-token-storage.service'

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(private router: Router, private tokenStorageService: AuthTokenStorageService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const currentUser = this.tokenStorageService.getUser()
    if (currentUser) {
      return true
    }

    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}})
    return false
  }

}

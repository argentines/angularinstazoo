import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {Observable} from 'rxjs'
import {environment} from 'src/environments/environment'

import {UserRegisterInputInterface} from 'src/app/auth/types/user_register_input_interface'
import {LoginRequestInterface} from 'src/app/auth/types/login-request_interface'

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {}

  login(user: LoginRequestInterface): Observable<any> {
    return this.http.post(`${environment.server_url}/auth/signin`, user)
  }

  register(user: UserRegisterInputInterface): Observable<any> {
    return this.http.post(`${environment.server_url}/auth/signup`, user)
  }
}

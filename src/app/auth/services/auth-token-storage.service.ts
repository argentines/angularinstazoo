import {Injectable} from '@angular/core'
import {environment} from 'src/environments/environment'
import {Observable} from 'rxjs'

@Injectable()
export class AuthTokenStorageService {

  public saveToken(token: string) {
    window.sessionStorage.removeItem(environment.token_key)
    window.sessionStorage.setItem(environment.token_key, token)
  }

  public getToken() {
    return window.sessionStorage.getItem(environment.token_key)
  }

  public saveUser(user) {
    window.sessionStorage.removeItem(environment.user_key)
    window.sessionStorage.setItem(environment.user_key, JSON.stringify(user))
  }

  public getUser(): Observable<string[]> {
    return JSON.parse(window.sessionStorage.getItem(environment.user_key))
  }

  public logOut() {
    window.sessionStorage.clear()
    window.location.reload()
  }

}

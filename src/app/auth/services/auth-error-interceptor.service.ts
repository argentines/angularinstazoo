import {Injectable} from '@angular/core'
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http'
import {catchError} from 'rxjs/operators'
import {Observable, throwError} from 'rxjs'

import {AuthTokenStorageService} from 'src/app/auth/services/auth-token-storage.service'
import {NotificationService} from 'src/app/shared/services/notification.service'

@Injectable()
export class AuthErrorInterceptorService implements HttpInterceptor{

  constructor(private authTokenStorage: AuthTokenStorageService,
              private notificationService: NotificationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(catchError(err => {
      if(err.status === 401) {
        this.authTokenStorage.logOut()
        window.location.reload()
      }

      const error = err.error.message || err.statusText
      this.notificationService.showSnackBar(error)
      return throwError(error)
    }))
  }
}

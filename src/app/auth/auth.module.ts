import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {HTTP_INTERCEPTORS} from '@angular/common/http'
import {ReactiveFormsModule} from '@angular/forms'
//Services
import {AuthService} from 'src/app/auth/services/auth.service'
import {AuthTokenStorageService} from 'src/app/auth/services/auth-token-storage.service'
import {AuthInterceptorService} from 'src/app/auth/services/auth-interceptor.service'
import {AuthErrorInterceptorService} from 'src/app/auth/services/auth-error-interceptor.service'
//Modules
import {AuthRoutingModule} from 'src/app/auth/auth-routing.module'
import {MaterialModule} from 'src/app/material-module'
import {RegisterFormModule} from 'src/app/shared/modules/register-form/register-form.module'
//Components
import {LoginComponent} from 'src/app/auth/components/login/login.component'
import {RegisterComponent} from 'src/app/auth/components/register/register.component'


@NgModule({
  declarations: [LoginComponent, RegisterComponent],
  exports: [LoginComponent, RegisterComponent],
  imports: [CommonModule,
            ReactiveFormsModule,
            MaterialModule,
            AuthRoutingModule,
            RegisterFormModule],
  providers: [
    AuthService,
    AuthTokenStorageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthErrorInterceptorService,
      multi: true
    }
  ]
})
export class AuthModule {}
